import { NgModule } from "@angular/core"
import { FormsModule } from "@angular/forms"
import { BrowserModule } from "@angular/platform-browser"
import { RouterModule, Routes } from "@angular/router"
import { ModalModule } from "angular2-modal"
import { BootstrapModalModule } from "angular2-modal/plugins/bootstrap"
import { Ng2PaginationModule } from "ng2-pagination"

import { AppComponent } from "./app.component"
import { routing } from "./app.routes"

//Home Component
import { HomeComponent } from "./home/home.component"

//Kanji Component
import { KanjiDetailComponent } from "./kanji/kanji-detail.component"
import { KanjiListComponent } from "./kanji/kanji-list.component"

//Login Component
import { LoginComponent } from "./login/login.component"

//Register Component
import { RegisterComponent } from "./register/register.component"

//NavBar Component
import { NavBarComponent } from "./navbar/navbar.component"

//Deck Component
import { DeckComponent } from "./deck/deck.component"
import { DeckDetailComponent } from "./deck/deck-detail.component"

//SRS Component
import { SRSStudyComponent } from "./srs/srs.component"

//Modal
import { AddDeckModal } from "./deck/add-deck.modal"
import { KanjiDeckModal } from "./kanji/kanji-deck.modal"

import { LocationStrategy, HashLocationStrategy } from '@angular/common'

@NgModule({
    // Components, Pipes, Directive
    declarations: [
        AppComponent,
        HomeComponent,
        KanjiListComponent,
        KanjiDetailComponent,
        LoginComponent,
        RegisterComponent,
        NavBarComponent,
        DeckComponent,
        AddDeckModal,
        KanjiDeckModal,
        DeckDetailComponent,
        SRSStudyComponent
    ],
    // Entry Components
    entryComponents: [
        AppComponent,
        AddDeckModal,
        KanjiDeckModal
    ],
    // Providers
    providers: [
      { provide: LocationStrategy, useClass: HashLocationStrategy }
    ],
    // Modules
    imports: [
        BrowserModule,
        FormsModule,
        Ng2PaginationModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
        routing
    ],
    // Main Component
    bootstrap: [AppComponent]
})

export class AppModule {
    constructor() { }
}
