import { Component } from '@angular/core'
import { globalEventsService } from "../_services/global-event.service"
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal'
import { BSModalContext } from 'angular2-modal/plugins/bootstrap'
import { parseService } from "../_services/parse.service"

import template from './add-deck.modal.html'

export class CustomModalContext extends BSModalContext {
  public num1: number;
  public num2: number;
}

/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
@Component({
  selector: 'modal-content',
  styles: [],
  template
})
export class AddDeckModal implements CloseGuard, ModalComponent<CustomModalContext> {
  context: CustomModalContext

  public wrongAnswer: boolean
  public shouldUseMyClass: boolean

  constructor(public dialog: DialogRef<CustomModalContext>) {
    this.context = dialog.context
  }

  addDeck(name) {
    parseService.addDeck(name).then((success) => {
      console.log(success)
      globalEventsService.refreshDeckList.emit(true)
    })
    this.dialog.close()
  }

  closeModal() {
    this.dialog.close()
    //console.log("masuk function")
  }
}
