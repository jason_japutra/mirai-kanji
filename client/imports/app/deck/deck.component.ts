import { Component, OnInit, ViewContainerRef, ViewEncapsulation } from "@angular/core"
import { parseService } from "../_services/parse.service"
import { globalEventsService } from "../_services/global-event.service"
import { Observable, Subscription } from "rxjs"
import { Overlay, overlayConfigFactory } from 'angular2-modal'
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap'
import { AddDeckModal } from './add-deck.modal'

import template from "./deck.component.html"
//import add_deck_modal from "./add-deck.modal.html"
import style from "./deck.component.scss"

@Component({
    selector: "deck",
    template,
    styles: [style]
})

export class DeckComponent implements OnInit{
    public isLoggedIn: boolean = false
    public deckList: any

    constructor(
        overlay: Overlay,
        vcRef: ViewContainerRef,
        public modal: Modal
    ) {
        overlay.defaultViewContainer = vcRef
        globalEventsService.refreshDeckList.subscribe((refresh) => {
          console.log(refresh)
          if(refresh == true){
            this.deckList = parseService.getDeck()
            globalEventsService.refreshDeckList.emit(false)
          }
        })
    }

    ngOnInit() {
      //console.log('init')
      this.deckList = parseService.getDeck()
      //console.log(this.deckList)
    }

    openModal() {
      // console.log(CustomModal)
        this.modal.open(AddDeckModal, overlayConfigFactory({  }, BSModalContext))
    }

    test() {
      console.log("masuk function")
    }
}
