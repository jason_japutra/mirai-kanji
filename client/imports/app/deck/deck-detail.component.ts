import { Component, OnInit, Input, OnChanges, SimpleChanges } from "@angular/core"
import { Observable, Subscription } from "rxjs"
import { parseService } from "../_services/parse.service"
import { ActivatedRoute, Params, Router } from "@angular/router"

import template from "./deck-detail.component.html"
import styles from "./deck-detail.component.scss"
// @import "angular2-busy/build/style/busy.css";

@Component({
    template,
    styles: [
        styles
    ]
})

export class DeckDetailComponent implements OnInit {
    model: any = {}
    deckDetail: Promise<any>
    id: Number
    loading = false

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.loading = false
        let sub = this.route.params.subscribe(params => {
            this.id = params["id"]; // (+) converts string "id" to a number

            // In a real app: dispatch action to load the details here.
        })

        this.deckDetail = parseService.getDeckDetail(this.id)
    }

    goBack() {
        this.router.navigate(["/deck"])
    }

    studyDeck() {
      this.router.navigate(["/srs",this.id])
    }

}
