import { Component, OnInit, Input, OnChanges, SimpleChanges } from "@angular/core"
import { Observable, Subscription } from "rxjs"
import { parseService } from "../_services/parse.service"
import { ActivatedRoute, Params, Router } from "@angular/router"

import template from "./srs.component.html"
import styles from "./srs.component.scss"
// @import "angular2-busy/build/style/busy.css";

@Component({
    template,
    styles: [
        styles
    ]
})

export class SRSStudyComponent implements OnInit {
    model: any = {}
    promise: Promise<any>
    id: number
    loading = false
    deckEntry: any
    curr: number = 0
    glyph: String = ""
    keyword: String = ""
    deckDetail: any
    showKanji: boolean = false

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.loading = false
        let sub = this.route.params.subscribe(params => {
            this.id = params["id"]; // (+) converts string "id" to a number

            // In a real app: dispatch action to load the details here.
        })

        this.promise = parseService.getDeckDetail(this.id).then(results => {
            this.loading = true
            console.log(results[this.curr])
            this.glyph = results[this.curr].kanji
            this.keyword = results[this.curr].keyword
            this.deckDetail = results

            return results
        })
        //console.log(this.deckDetail)
    }

    goBack() {
        this.router.navigate(["/deck"])
    }

    goNext() {
        if (!this.showKanji) {
            this.showKanji = true
            this.glyph = this.deckDetail[this.curr].kanji
            this.curr++
            //console.log(this.curr)
        } else {
            if (this.curr == this.deckDetail.length) {
                this.curr = 0
            }
            this.showKanji = false
            this.keyword = this.deckDetail[this.curr].keyword
        }
    }

    goPrev() {
        if (!this.showKanji) {
            this.showKanji = true
            this.glyph = this.deckDetail[this.curr].kanji
            this.curr--
            //console.log(this.curr)
        } else {
            if (this.curr < 0) {
                this.curr = this.deckDetail.length - 1
            }
            this.showKanji = false
            this.keyword = this.deckDetail[this.curr].keyword
        }
    }

}
