"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ParseService = (function () {
    function ParseService() {
        this.Parse = require("parse");
        this.Parse.initialize("mirai-apps-miraikanji");
        this.Parse.serverURL = "http://miraidev.us-east-1.elasticbeanstalk.com/miraikanji";
    }
    ParseService.prototype.registerUser = function (username, password, firstName, lastName, user_email) {
        return this.Parse.User.signUp(username, password, {
            first_name: firstName, last_name: lastName, email: user_email
        });
    };
    ParseService.prototype.loginUser = function (username, password) {
        return this.Parse.User.logIn(username, password);
    };
    ParseService.prototype.register = function (username, password, data) {
        return this.Parse.User.signUp(username, password, {
            firstName: data.firstName, lastName: data.lastName, email: data.user_email
        });
    };
    ParseService.prototype.getId = function () {
        return this.Parse.User.id;
    };
    ParseService.prototype.logout = function () {
        return this.Parse.User.logOut();
    };
    ParseService.prototype.getItemPage = function (page, perPage) {
        var user = this.Parse.User.current();
        var glyph = this.Parse.Object.extend("Glyph");
        var story = this.Parse.Object.extend("User_Story");
        var query = new this.Parse.Query(glyph);
        var query_story = new this.Parse.Query(story);
        query.limit(perPage);
        query.skip((page - 1) * perPage);
        query.ascending("gID");
        var data = {
            total: 0,
            list: []
        };
        return query.count().then(function (count) {
            data.total = count;
            return query.find();
        }).then(function (glyphs) {
            var contentArr = [];
            var idArr = [];
            for (var _i = 0, glyphs_1 = glyphs; _i < glyphs_1.length; _i++) {
                var kanji = glyphs_1[_i];
                var contentData = {
                    id: kanji.get("gID"),
                    kanji: kanji.get("kanji"),
                    keyword: kanji.get("keyword")
                };
                contentArr.push(contentData);
                idArr.push(+kanji.get("gID"));
            }
            query_story.containedIn("gID", idArr);
            query_story.include("user");
            query_story.equalTo("user", {
                __type: "Pointer",
                className: "_User",
                objectId: user.id
            });
            return query_story.find().then(function (stories) {
                console.log(stories);
                var addToDict = function (arr, id, story) {
                    for (var _i = 0, arr_1 = arr; _i < arr_1.length; _i++) {
                        var obj = arr_1[_i];
                        if (obj.id === +id) {
                            obj.story = story.toString();
                            return;
                        }
                    }
                };
                for (var _i = 0, stories_1 = stories; _i < stories_1.length; _i++) {
                    var str = stories_1[_i];
                    var id = str.get("gID");
                    var story_1 = str.get("story");
                    addToDict(contentArr, id, story_1);
                }
                data.list = contentArr;
                return data;
            });
        });
    };
    ParseService.prototype.getDetail = function (gID) {
        var glyph = this.Parse.Object.extend("Glyph");
        var query = new this.Parse.Query(glyph);
        query.equalTo("gID", +gID);
        return query.first().then(function (dt) {
            var detailData = {
                id: dt.id,
                kanji: dt.get("kanji"),
                keyword: dt.get("keyword"),
                glyph_id: dt.get("gID")
            };
            return detailData;
        });
    };
    ParseService.prototype.getStory = function (gID) {
        var user = this.Parse.User.current();
        var story = this.Parse.Object.extend("User_Story");
        var query_story = new this.Parse.Query(story);
        query_story.include("user");
        query_story.equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: user.id
        });
        query_story.equalTo("gID", +gID);
        return query_story.count().then(function (count) {
            if (count === 0) {
                return "\u00A0\u00A0";
            }
            else {
                return query_story.first().then(function (dt) {
                    return dt.get("story");
                });
            }
        });
    };
    ParseService.prototype.addStory = function (id, story, gID) {
        var _this = this;
        var user = this.Parse.User.current();
        var storyObject = this.Parse.Object.extend("User_Story");
        var query_story = new this.Parse.Query(storyObject);
        query_story.include("user");
        query_story.equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: user.id
        });
        query_story.equalTo("gID", +gID);
        return query_story.count().then(function (count) {
            if (count !== 0) {
                return query_story.first().then(function (dt) {
                    dt.set("story", story);
                    return dt.save().then(function (story) {
                        return story.get("story");
                    }, function (err) {
                        console.log(err);
                    });
                });
            }
            else {
                var item = new _this.Parse.Object("User_Story");
                item.set("gID", +gID);
                item.set("story", story);
                item.set("glyph", {
                    __type: "Pointer",
                    className: "Glyph",
                    objectId: id
                });
                item.set("user", {
                    __type: "Pointer",
                    className: "_User",
                    objectId: user.id
                });
                return item.save();
            }
        });
    };
    return ParseService;
}());
ParseService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], ParseService);
exports.parseService = new ParseService();
//# sourceMappingURL=parse.service.js.map