import { Injectable } from "@angular/core";

@Injectable()
class ParseService {
    public Parse = require("parse")
    public isLoggedIn: boolean = false

    constructor() {
        this.Parse.initialize("mirai-apps-miraikanji");
        this.Parse.serverURL = "http://miraidev.us-east-1.elasticbeanstalk.com/miraikanji"
    }

    //
    // Account
    //

    public registerUser(username, password, firstName, lastName, user_email) {
        return this.Parse.User.signUp(username, password, {
            first_name: firstName, last_name: lastName, email: user_email
        })
    }

    public loginUser(username, password) {
        this.isLoggedIn = true
        console.log(this.isLoggedIn)
        return this.Parse.User.logIn(username, password)
    }

    public register(username, password, data) {
        return this.Parse.User.signUp(username, password, {
            firstName: data.firstName, lastName: data.lastName, email: data.user_email
        })
    }

    public getId() {
        return this.Parse.User.id
    }

    public getUser() {
        return this.Parse.User.current()
    }

    public logout() {
        this.isLoggedIn = false
        console.log(this.isLoggedIn)
        return this.Parse.User.logOut()
    }

    //
    // Glyph
    //

    public getItemPage(page: number, perPage: number) {
        let user = this.Parse.User.current()

        let glyph = this.Parse.Object.extend("Glyph")
        let story = this.Parse.Object.extend("User_Story")

        let query = new this.Parse.Query(glyph)
        let query_story = new this.Parse.Query(story)

        query.limit(perPage)
        query.skip((page - 1) * perPage)
        query.ascending("gID")

        let data = {
            total: 0,
            list: []
        }

        return query.count().then((count) => {
            data.total = count
            return query.find()
        }).then((glyphs) => {

            let contentArr = []
            let idArr = []

            for (let kanji of glyphs) {
                let contentData = {
                    id: kanji.get("gID"),
                    kanji: kanji.get("kanji"),
                    keyword: kanji.get("keyword"),
                    object_id: kanji.id
                }
                contentArr.push(contentData)
                idArr.push(+kanji.get("gID"))
            }

            query_story.containedIn("gID", idArr)
            query_story.include("user")
            query_story.equalTo("user", {
                __type: "Pointer",
                className: "_User",
                objectId: user.id
            })

            return query_story.find().then((stories) => {
                console.log(stories);
                let addToDict = (arr, id, story) => {
                    for (let obj of arr) {
                        if (obj.id === +id) {
                            obj.story = story.toString();
                            return;
                        }
                    }
                }

                for (let str of stories) {
                    let id = str.get("gID")
                    let story = str.get("story")
                    addToDict(contentArr, id, story)
                }

                data.list = contentArr

                return data
            })
        })
    }

    public getKanjiDetail(gID) {
        let glyph = this.Parse.Object.extend("Glyph")
        let query = new this.Parse.Query(glyph)

        query.equalTo("gID", +gID)

        return query.first().then((dt) => {
            let detailData = {
                id: dt.id,
                kanji: dt.get("kanji"),
                keyword: dt.get("keyword"),
                glyph_id: dt.get("gID")
            }
            return detailData
        })
    }

    public getStory(gID) {
        let user = this.Parse.User.current()
        let story = this.Parse.Object.extend("User_Story")
        let query_story = new this.Parse.Query(story)

        query_story.include("user")
        query_story.equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: user.id
        })
        query_story.equalTo("gID", +gID)

        return query_story.count().then((count) => {
            if (count === 0) {
                return "\u00A0\u00A0"
            } else {
                return query_story.first().then((dt) => {
                  console.log(dt.get("story"))
                    return dt.get("story")
                })
            }
        })
    }

    public getDeck() {
        let user = this.Parse.User.current()
        let deck = this.Parse.Object.extend("User_Deck")

        let query = new this.Parse.Query(deck)
        let deckArr = []
        query.include("user")
        query.equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: user.id
        })
        return query.find().then((results) => {
            for (let data of results) {
                let deckData = {
                    name: data.get("name"),
                    id: data.id,
                    list: data.get("list")
                }
                deckArr.push(deckData)
            }
            return deckArr
        })

    }

    public getDeckDetail(deckID) {
      let user = this.Parse.User.current()
      let user_deck = this.Parse.Object.extend("User_Deck")
      let query = new this.Parse.Query(user_deck)

      query.include("list")
      //query.equalTo("list", deckID)

      return query.first().then((dt) => {
        let detailList = []
        let list = dt.get("list")

        for ( let result of list ) {
          let detailData = {
            keyword: result.get('keyword'),
            kanji: result.get('kanji'),
            id: result.id
          }
          detailList.push(detailData)
        }
        return detailList
      })
    }

    public addStory(id, story, gID) {
        let user = this.Parse.User.current()
        let storyObject = this.Parse.Object.extend("User_Story")
        let query_story = new this.Parse.Query(storyObject)

        query_story.include("user")
        query_story.equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: user.id
        })
        query_story.equalTo("gID", +gID)

        return query_story.count().then((count) => {
            if (count !== 0) {
                return query_story.first().then((dt) => {
                    dt.set("story", story)
                    return dt.save().then((story) => {
                        return story.get("story")
                    }, (err) => {
                        console.log(err)
                    })
                })
            } else {
                let item = new this.Parse.Object("User_Story")
                item.set("gID", +gID)
                item.set("story", story)
                item.set("glyph", {
                    __type: "Pointer",
                    className: "Glyph",
                    objectId: id
                })
                item.set("user", {
                    __type: "Pointer",
                    className: "_User",
                    objectId: user.id
                })
                return item.save()
            }
        })
    }

    public addDeck(name) {
        let user = this.Parse.User.current()
        let deck = new this.Parse.Object("User_Deck")
        deck.set("name", name)
        deck.set("user", {
            __type: "Pointer",
            className: "_User",
            objectId: user.id
        })
        deck.set("list", [])
        return deck.save()
    }

    public addToDeck(deck_id, glyph_id) {
        let user = this.Parse.User.current()
        let deck = new this.Parse.Object("User_Deck")
        console.log(deck_id + " " + glyph_id)
        //let glyph_array = []
        deck.set('id', deck_id)
        deck.addUnique("list",  {
            __type: "Pointer",
            className: "Glyph",
            objectId: glyph_id
        })
        return deck.save()
    }

}

export const parseService = new ParseService()
