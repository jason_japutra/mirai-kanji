import { EventEmitter, Injectable} from "@angular/core";

@Injectable()
class GlobalEventsService {
    public showNavBar: EventEmitter<boolean> = new EventEmitter()
    public refreshDeckList: EventEmitter<boolean> = new EventEmitter()

    constructor() {

    }
}

export const globalEventsService = new GlobalEventsService()
