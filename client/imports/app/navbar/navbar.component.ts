import { Component, OnInit } from "@angular/core"
import { parseService } from "../_services/parse.service"
import { globalEventsService } from "../_services/global-event.service"
import { Observable, Subscription } from "rxjs"

import template from "./navbar.component.html"
import style from "./navbar.component.scss"

@Component({
    selector: "navbar",
    template,
    styles: [style]
})

export class NavBarComponent {
    user: any
    check: Observable<any>
    loggedIn: boolean = false
    observer: Observable<any>

    constructor(

    ) {
        if(parseService.getUser() != null){
          this.loggedIn = true
          console.log('logged in')
        }else
          this.loggedIn = false

        globalEventsService.showNavBar.subscribe((mode) => {
            this.loggedIn = mode
            console.log(mode)
        })


        // this.observer = Observable.of(parseService.isLoggedIn).subscribe(
        //     res => {
        //       this.loggedIn = res
        //       console.log(res)
        //     },
        //     error => console.log(error),
        //     () => console.log('finished')
        // )

        // console.log(this.observer)
    }
}
