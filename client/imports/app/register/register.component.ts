import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { parseService } from "../_services/parse.service"

import template from "./register.component.html"

@Component({
    template
})

export class RegisterComponent implements OnInit {
    model: any = {}
    loading = false

    constructor(
        private router: Router,
        // private authenticationService: AuthenticationService,
        // private alertService: AlertService

    ) { }

    ngOnInit() {
        console.log("register form init")
    }

    register() {
        // console.log(this.model.firstName, this.model.lastName, this.model.username, this.model.password)
        parseService.registerUser(this.model.username, this.model.password, this.model.firstName, this.model.lastName, this.model.email)
            .then(success => {
                console.log(success)
                this.router.navigate(["/login"])
            },
            err => {
                console.log(err)
            })
    }
}
