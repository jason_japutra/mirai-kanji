"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var parse_service_1 = require("../_services/parse.service");
var register_component_html_1 = require("./register.component.html");
var RegisterComponent = (function () {
    function RegisterComponent(router) {
        this.router = router;
        this.model = {};
        this.loading = false;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        console.log("register form init");
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        parse_service_1.parseService.registerUser(this.model.username, this.model.password, this.model.firstName, this.model.lastName, this.model.email)
            .then(function (success) {
            console.log(success);
            _this.router.navigate(["/login"]);
        }, function (err) {
            console.log(err);
        });
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    core_1.Component({
        template: register_component_html_1.default
    }),
    __metadata("design:paramtypes", [router_1.Router])
], RegisterComponent);
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map