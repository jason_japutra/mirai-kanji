import { Routes, RouterModule } from "@angular/router"

import { HomeComponent } from "./home/home.component"
import { LoginComponent } from "./login/login.component"
import { RegisterComponent } from "./register/register.component"
import { KanjiListComponent } from "./kanji/kanji-list.component"
import { KanjiDetailComponent } from "./kanji/kanji-detail.component"
import { DeckComponent } from "./deck/deck.component"
import { DeckDetailComponent } from "./deck/deck-detail.component"
import { SRSStudyComponent } from "./srs/srs.component"

const appRoutes: Routes = [
    { path: "", component: HomeComponent},
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "kanji", component: KanjiListComponent },
    { path: "kanji-detail/:id", component: KanjiDetailComponent },
    { path: "deck", component: DeckComponent },
    { path: "deck-detail/:id", component: DeckDetailComponent },
    { path: "srs/:id", component: SRSStudyComponent}
]

export const routing = RouterModule.forRoot(appRoutes)
