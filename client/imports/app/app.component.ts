import { Component, OnInit } from "@angular/core"
import { parseService } from "./_services/parse.service"
import { Observable, Subscription } from "rxjs"

import template from "./app.component.html"
import style from "./app.component.scss"

@Component({
    selector: "app",
    template,
    styles: [style]
})

export class AppComponent implements OnInit {

    constructor() {

    }


    ngOnInit() {

    }
}
