import { Component, OnInit, Input, ViewContainerRef, ViewEncapsulation } from "@angular/core"
import { Observable, Subscription } from "rxjs"
import { parseService } from "../_services/parse.service"
import { Router, ActivatedRoute } from "@angular/router"
import { globalEventsService } from "../_services/global-event.service"
import { Overlay, overlayConfigFactory } from 'angular2-modal'
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap'
import { KanjiDeckModal } from './kanji-deck.modal'

import template from "./kanji-list.component.html";
import styles from "./parse.less";

@Component({
    template,
    styles: [
        styles
    ]
})

export class KanjiListComponent implements OnInit {
    busy: Promise<any>
    p: number = 1
    total: number
    loading: boolean = false

    @Input("data") list: any = [];
    listAsync: Observable<any>

    constructor(
        private router: Router,
        overlay: Overlay,
        vcRef: ViewContainerRef,
        public modal: Modal
    ) {
        overlay.defaultViewContainer = vcRef
    }

    ngOnInit() {
        this.getPage(1)
    }

    getPage(page: number) {
        this.loading = true
        this.listAsync = serverCall(page)
            .do(res => {
                this.total = res.total
                this.p = page
                this.loading = false
            })
            .map(res => res.list)
    }

    logout() {
        parseService.logout().then((success) => {
            this.router.navigate(["/login"])
            globalEventsService.showNavBar.emit(false)
        })
    }

    viewDetails(data) {
        this.router.navigate(["/kanji-detail", data])
    }

    openModal(glyph_id) {
        this.modal.open(KanjiDeckModal, overlayConfigFactory({ gID: glyph_id }, BSModalContext))
    }

}

function serverCall(page: number) {
    const perPage = 10
    const start = (page - 1) * perPage
    const end = start + perPage

    return Observable.fromPromise(parseService.getItemPage(page, perPage).then((success) => {
        return success
    })).delay(1000);
}
