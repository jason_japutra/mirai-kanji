import { Component } from '@angular/core'
import { globalEventsService } from "../_services/global-event.service"
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal'
import { BSModalContext } from 'angular2-modal/plugins/bootstrap'
import { parseService } from "../_services/parse.service"

import template from './kanji-deck.modal.html'

export class CustomModalContext extends BSModalContext {
  public gID: number
}

/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
@Component({
  selector: 'modal-content',
  styles: [],
  template
})

export class KanjiDeckModal implements CloseGuard, ModalComponent<CustomModalContext> {
  context: CustomModalContext

  public wrongAnswer: boolean
  public shouldUseMyClass: boolean

  constructor(public dialog: DialogRef<CustomModalContext>) {
    this.context = dialog.context
  }

  ngOnInit() {
    this.deckList = parseService.getDeck()
  }

  closeModal() {
    this.dialog.close()
  }

  addToDeck(deck_id) {
    parseService.addToDeck(deck_id, this.context.gID)
    this.dialog.close()
  }
  
}
