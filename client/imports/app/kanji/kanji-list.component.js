"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var parse_service_1 = require("../_services/parse.service");
var router_1 = require("@angular/router");
var kanji_list_component_html_1 = require("./kanji-list.component.html");
var parse_less_1 = require("./parse.less");
var KanjiListComponent = (function () {
    function KanjiListComponent(router) {
        this.router = router;
        this.p = 1;
        this.loading = false;
        this.list = [];
    }
    KanjiListComponent.prototype.ngOnInit = function () {
        this.getPage(1);
    };
    KanjiListComponent.prototype.getPage = function (page) {
        var _this = this;
        this.loading = true;
        this.listAsync = serverCall(page)
            .do(function (res) {
            _this.total = res.total;
            _this.p = page;
            _this.loading = false;
        })
            .map(function (res) { return res.list; });
    };
    KanjiListComponent.prototype.logout = function () {
        var _this = this;
        parse_service_1.parseService.logout().then(function (success) {
            _this.router.navigate(["/login"]);
        });
    };
    KanjiListComponent.prototype.viewDetails = function (data) {
        this.router.navigate(["/kanji-detail", data]);
    };
    return KanjiListComponent;
}());
__decorate([
    core_1.Input("data"),
    __metadata("design:type", Object)
], KanjiListComponent.prototype, "list", void 0);
KanjiListComponent = __decorate([
    core_1.Component({
        template: kanji_list_component_html_1.default,
        styles: [
            parse_less_1.default
        ]
    }),
    __metadata("design:paramtypes", [router_1.Router])
], KanjiListComponent);
exports.KanjiListComponent = KanjiListComponent;
function serverCall(page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable.fromPromise(parse_service_1.parseService.getItemPage(page, perPage).then(function (success) {
        return success;
    })).delay(1000);
}
//# sourceMappingURL=kanji-list.component.js.map