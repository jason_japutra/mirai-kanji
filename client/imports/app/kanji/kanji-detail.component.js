"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var parse_service_1 = require("../_services/parse.service");
var router_1 = require("@angular/router");
var kanji_detail_component_html_1 = require("./kanji-detail.component.html");
var parse_less_1 = require("./parse.less");
var KanjiDetailComponent = (function () {
    function KanjiDetailComponent(route, router) {
        this.route = route;
        this.router = router;
        this.model = {};
        this.loading = false;
    }
    KanjiDetailComponent.prototype.getStory = function () {
        var _this = this;
        return parse_service_1.parseService.getStory(this.id).then(function (story) {
            _this.loading = true;
            console.log(story);
            return story;
        }, function (err) {
            console.log(err);
            return "-";
        });
    };
    KanjiDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = false;
        var sub = this.route.params.subscribe(function (params) {
            _this.id = +params["id"];
            return _this.id;
        });
        this.detail = parse_service_1.parseService.getDetail(this.id);
        this.story = this.getStory();
    };
    KanjiDetailComponent.prototype.goBack = function () {
        this.router.navigate(["/kanji"]);
    };
    KanjiDetailComponent.prototype.onSubmit = function (id, story, gID) {
        console.log(id);
        console.log(story);
        this.story = parse_service_1.parseService.addStory(id, story, gID);
    };
    return KanjiDetailComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Promise)
], KanjiDetailComponent.prototype, "story", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Promise)
], KanjiDetailComponent.prototype, "tempStory", void 0);
KanjiDetailComponent = __decorate([
    core_1.Component({
        template: kanji_detail_component_html_1.default,
        styles: [
            parse_less_1.default
        ]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        router_1.Router])
], KanjiDetailComponent);
exports.KanjiDetailComponent = KanjiDetailComponent;
//# sourceMappingURL=kanji-detail.component.js.map