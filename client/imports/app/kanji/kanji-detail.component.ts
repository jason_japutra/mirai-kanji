import { Component, OnInit, Input, OnChanges, SimpleChanges } from "@angular/core"
import { Observable, Subscription } from "rxjs"
import { parseService } from "../_services/parse.service"
import { ActivatedRoute, Params, Router } from "@angular/router";

import template from "./kanji-detail.component.html";
import styles from "./parse.less";
// @import "angular2-busy/build/style/busy.css";

@Component({
    template,
    styles: [
        styles
    ]
})

export class KanjiDetailComponent implements OnInit {
    model: any = {}
    detail: Promise<any>
    id: Number
    loading = false
    @Input() story: Promise<any>
    @Input() tempStory: Promise<any>

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    public getStory() {
        // console.log("get story")
        return parseService.getStory(this.id).then(story => {
            this.loading = true
            console.log(story)
            return story
        }, err => {
            console.log(err)
            return "-"
        })
    }

    ngOnInit() {
        this.loading = false
        let sub = this.route.params.subscribe(params => {
            this.id = +params["id"]; // (+) converts string "id" to a number

            return this.id
            // In a real app: dispatch action to load the details here.
        });

        this.detail = parseService.getKanjiDetail(this.id)

        this.story = this.getStory()



        // console.log(this.detail)
    }

    goBack() {
        this.router.navigate(["/kanji"])
    }

    onSubmit(id, story, gID) {
        console.log(id)
        console.log(story)
        this.story = parseService.addStory(id, story, gID)
    }

}
