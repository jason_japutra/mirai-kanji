"use strict";
var router_1 = require("@angular/router");
var login_component_1 = require("./login/login.component");
var register_component_1 = require("./register/register.component");
var kanji_list_component_1 = require("./kanji/kanji-list.component");
var kanji_detail_component_1 = require("./kanji/kanji-detail.component");
var appRoutes = [
    { path: "", component: login_component_1.LoginComponent },
    { path: "login", component: login_component_1.LoginComponent },
    { path: "register", component: register_component_1.RegisterComponent },
    { path: "kanji", component: kanji_list_component_1.KanjiListComponent },
    { path: "kanji-detail/:id", component: kanji_detail_component_1.KanjiDetailComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routes.js.map