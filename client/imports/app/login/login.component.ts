import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { parseService } from "../_services/parse.service"
import { globalEventsService } from "../_services/global-event.service"

import template from "./login.component.html"

@Component({
    template
})

export class LoginComponent implements OnInit {
    model: any = {}
    loading = false
    // parseService : ParseService = new ParseService

    constructor(
        private router: Router
    ) { }

    ngOnInit() {
        // reset login status
        // this.authenticationService.logout();
        // console.log("init")
    }

    login() {
        this.loading = true
        parseService.loginUser(this.model.username, this.model.password)
            .then(success => {
                globalEventsService.showNavBar.emit(true)
                this.loading = false
                this.router.navigate(["/kanji"])
                //localStorage.setItem("currentUser", ParseService.getId())
            }, err => {
                this.loading = false
                console.log(err)
            })
    }

    // this.loading = true;
    // this.authenticationService.login(this.model.username, this.model.password)
    //     .subscribe(
    //         data => {
    //             this.router.navigate(["/"]);
    //         },
    //         error => {
    //             this.alertService.error(error);
    //             this.loading = false;
    //         });

}
