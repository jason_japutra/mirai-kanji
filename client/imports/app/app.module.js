"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var ng2_pagination_1 = require("ng2-pagination");
var app_component_1 = require("./app.component");
var app_routes_1 = require("./app.routes");
var kanji_detail_component_1 = require("./kanji/kanji-detail.component");
var kanji_list_component_1 = require("./kanji/kanji-list.component");
var login_component_1 = require("./login/login.component");
var register_component_1 = require("./register/register.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.AppComponent,
            kanji_list_component_1.KanjiListComponent,
            kanji_detail_component_1.KanjiDetailComponent,
            login_component_1.LoginComponent,
            register_component_1.RegisterComponent
        ],
        entryComponents: [
            app_component_1.AppComponent
        ],
        providers: [],
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            ng2_pagination_1.Ng2PaginationModule,
            app_routes_1.routing
        ],
        bootstrap: [app_component_1.AppComponent]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map